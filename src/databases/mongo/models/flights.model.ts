import mongoose, { Schema } from 'mongoose';

interface Flight {
    code: string;
    origin: string;
    destination: string;
    status: string;
    passengers:[]
}



const schema = new Schema<Flight>(
    {
        code: { required: true, type: String },
        origin: { required: true, type: String },
        destination: { required: true, type: String },
        status: String,
        passengers: []
    },
    { timestamps: true },
);



export const FlightsModel = mongoose.model('Flights', schema);
