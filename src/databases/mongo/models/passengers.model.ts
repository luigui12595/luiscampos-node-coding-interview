import mongoose, { Schema } from 'mongoose';

interface Passenger {
    code: string;
    name: string;
}

const schema = new Schema<Passenger>(
    {
        code: { required: true, type: String },
        name: { required: true, type: String }
    },
    { timestamps: true },
);

export const PassengersModel = mongoose.model('Passengers', schema);