import { Database } from '../database_abstract';
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

import { FlightsModel } from './models/flights.model';
import { PassengersModel } from './models/passengers.model'

export class MongoStrategy extends Database {
    constructor() {
        super();
        this.getInstance();
    }

    private async getInstance() {
        const mongo = await MongoMemoryServer.create();
        const uri = mongo.getUri();

        const mongooseOpts = {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        };

        const flights = [
            {
                code: 'ABC-123',
                origin: 'EZE',
                destination: 'LDN',
                status: 'ACTIVE',
                passengers: []
            },
            {
                code: 'XYZ-678',
                origin: 'CRC',
                destination: 'MIA',
                status: 'ACTIVE',
                passengers: []
            },
        ];

        const passengers = [
            {
                code: "PSG-123",
                name: "Luis Campos"
            },
            {
                code: "PSG-321",
                name: "Maria Gonzalez"
            }
        ];

        (async () => {
            await mongoose.connect(uri, mongooseOpts);
            await FlightsModel.create(flights);
            await PassengersModel.create(passengers);
        })();
    }


    public async getFlights() {
        return FlightsModel.find();
    }

    public async addFlight(flight: {
        code: string;
        origin: string;
        destination: string;
        status: string;
    }) {
        return await FlightsModel.create(flight);
    }

    public async updateFlightStatus(code: string, flight: { code: string; origin: string; destination: string; status: string; }) {
        return await FlightsModel.updateOne({code},flight);
    }

    public async addPassenger(code: string, passengerCode: string){
        let flight = await FlightsModel.findOne({code});
        if (!flight) {
            return null
        }
        const passenger = await PassengersModel.findOne({code: passengerCode})
        if (!passenger){
            return null
        }
        flight.passengers.push(passenger);
        console.log(flight)
        return await FlightsModel.updateOne({code},flight)
    }

}
